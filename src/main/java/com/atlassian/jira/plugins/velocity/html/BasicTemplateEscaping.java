package com.atlassian.jira.plugins.velocity.html;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

/**
 * Action demonstrating the usage of automatic html escaping from velocity templates.
 */
public class BasicTemplateEscaping extends JiraWebActionSupport
{

    public BasicTemplateEscaping()
    {
    }

    @Override
    protected String doExecute() throws Exception
    {
        return "success";
    }

    public String getContent()
    {
        return "<script>alert('html escaped by default')</script>";
    }

    public String getFragmentHtml()
    {
        return "<span style='color:blue'>" +
                    "This method call should not be escaped, therefore rendering in blue colour" +
                "</span>";
    }

    @HtmlSafe
    public String getFragment()
    {
        return "<span style='color:grey'>" +
                "This method call should not be escaped, therefore rendering in grey colour" +
                "</span>";
    }
}
